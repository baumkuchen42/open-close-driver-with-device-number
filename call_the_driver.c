#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

char* DEVICE_PATH = "/dev/openclose";

int main (int argc, char **argv) {
	int device_handle = open(DEVICE_PATH, O_RDONLY);
	printf("Opened driver, file descriptor is: %i", device_handle);
	sleep(5);
	close(device_handle);
}
